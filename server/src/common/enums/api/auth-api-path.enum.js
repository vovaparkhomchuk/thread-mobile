const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  STATUS: '/status',
  UPDATE: '/update'
};

export { AuthApiPath };
