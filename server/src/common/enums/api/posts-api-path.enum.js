const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  DELETE: '/delete'
};

export { PostsApiPath };
