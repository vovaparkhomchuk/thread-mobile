import { Abstract } from '../abstract/abstract.repository';

class User extends Abstract {
  constructor({ userModel }) {
    super(userModel);
  }

  addUser(user) {
    return this.create(user);
  }

  update(userId, imageId) {
    return this.model.query().patchAndFetchById(userId, imageId);
  }

  getByEmail(email) {
    return this.model.query().select().where({ email }).first();
  }

  getByUsername(username) {
    return this.model.query().select().where({ username }).first();
  }

  getUserById(id) {
    return this.model.query()
      .select('id', 'createdAt', 'email', 'updatedAt', 'username')
      .where({ id })
      .withGraphFetched('[image]')
      .first();
  }
}

export { User };
