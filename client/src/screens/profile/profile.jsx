import * as React from 'react';
import { ButtonVariant, IconName, NotificationMessage, UserPayloadKey } from 'common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { Button, Image, Input, Stack, View } from 'components/components';
import { useAppForm, useDispatch, useSelector, useState } from 'hooks/hooks';
import { profileActionCreator } from 'store/actions';
import styles from './styles';
import { pickImage } from '../../helpers/image/pick-image/pick-image.helper';
import { image as imageService, notification as notificationService } from '../../services/services';

const Profile = () => {
  const dispatch = useDispatch();
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const { control, errors } = useAppForm({
    defaultValues: {
      [UserPayloadKey.USERNAME]: user?.username,
      [UserPayloadKey.EMAIL]: user?.email
    }
  });
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  const handleUserLogout = () => dispatch(profileActionCreator.logout());

  const handleUploadFile = async () => {
    setIsUploading(true);

    try {
      const data = await pickImage();
      if (!data) {
        return;
      }

      const { id, link } = await imageService.uploadImage(data);
      setImage({ id, link });
    } catch {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    } finally {
      setIsUploading(false);
    }
  };

  const handleUpdateProfile = () => {
    dispatch(profileActionCreator.updateProfile({ imageId: image?.id }));
  };

  if (!user) {
    return <></>;
  }

  return (
    <View style={styles.screen}>
      <View style={styles.content}>
        <Image
          style={styles.avatar}
          accessibilityIgnoresInvertColors
          source={{ uri: image?.link ?? user.image?.link ?? DEFAULT_USER_AVATAR }}
        />
        <Button
          title="Attach image"
          variant={ButtonVariant.TEXT}
          icon={IconName.PLUS_SQUARE}
          isLoading={isUploading}
          onPress={handleUploadFile}
        />
        {image?.link && (
          <Button
            title="Update profile"
            variant={ButtonVariant.TEXT}
            // icon={IconName.PLUS_SQUARE}
            isLoading={isUploading}
            onPress={handleUpdateProfile}
          />
        )}
        <Stack space={15}>
          <Input
            name={UserPayloadKey.USERNAME}
            control={control}
            errors={errors}
            iconName={IconName.USER}
            isDisabled
          />
          <Input
            name={UserPayloadKey.EMAIL}
            control={control}
            errors={errors}
            iconName={IconName.ENVELOPE}
            isDisabled
          />
          <Button title="Logout" onPress={handleUserLogout} />
        </Stack>
      </View>
    </View>
  );
};

export default Profile;
